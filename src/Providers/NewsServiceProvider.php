<?php

namespace Ls88\News\Providers;

use Illuminate\Support\ServiceProvider;
use Ls88\News\Console\Commands\NewsCommand;

class NewsServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public function boot()
    {
        // регистрация роутов
        $this->loadRoutesFrom(__DIR__.'/../routes/news.php');

        // регистрация шаблонов
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'news');

        // регистрация миграции
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');

        // публикация шаблонов для модификаций
        $this->publishes([
            __DIR__.'/../resources/views' => resource_path('views/vendor/news'),
        ]);

        // публикация css, js, images
        $this->publishes([
            __DIR__.'/../public' => public_path('vendor/news'),
        ], 'public');

        // публикация конфигурации пакета
        $this->publishes([
            __DIR__.'/../config/news.php' => config_path('news.php'),
        ]);

        // регистрация консольных команд пакета
        if ($this->app->runningInConsole()) {
            $this->commands([
                NewsCommand::class,
            ]);
        }
    }
}
