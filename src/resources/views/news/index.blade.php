@extends('vendor.news.news.layouts.app')

@section('content')
    <div class="content">
        @foreach($posts as $post)
            <div class="news">
                <div class="item">
                    <h1><a href="/news/{{ $post->slug }}">{{ $post->title }}</a></h1>
                    <span>{{ $post->created_at }}</span>
                </div>

                <p>{{ $post->description }}</p>
            </div>
        @endforeach
    </div>
@endsection
