<?php

use Illuminate\Support\Facades\Route;
use Ls88\News\Http\Controllers\News\PostsController;

Route::get('/news', [PostsController::class, 'index']);
