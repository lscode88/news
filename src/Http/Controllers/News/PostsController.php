<?php

namespace Ls88\News\Http\Controllers\News;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Ls88\News\Models\News;

class PostsController extends Controller
{
    public function index()
    {
        //dd(config('news.variable'));

        $posts = News::all();

        return view('news::news.index', [
            'posts' => $posts
        ]);
    }
}
